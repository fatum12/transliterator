(function($, standards) {
	'use strict';

	var $source,
		$target,
		$standard,
		$space,
		$register,
		$alphanum,
		$view;

	var SPACE_DASH = 1,			// заменять пробелы знаком дефиса
		SPACE_UNDERLINE = 2,	// заменять пробелы знаком подчеркивания
		REGISTER_LOWER = 1,		// приводить символы к нижнему регистру
		REGISTER_UPPER = 2;		// приводить символы к верхнему регистру

	$(document).ready(function() {
		$source = $('#text-source');
		$target = $('#text-target');
		$standard = $('#standard');
		$space = $('#space');
		$register = $('#register');
		$alphanum = $('#alphanum');
		$view = $('#standard-view');

		buildStandardSelect();
		buildStandardsInfo();

		autosize($source);
		$source.on('autosize:resized', function () {
			$target.height($source.height());
		});

		$source.simplyCountable({
			counter: '#source-count',
			countType: 'characters',
			maxCount: null,
			strictMax: false,
			countDirection: 'up',
			safeClass: 'safe',
			overClass: 'over',
			thousandSeparator: ' '
		});

		$target.simplyCountable({
			counter: '#target-count',
			countType: 'characters',
			maxCount: null,
			strictMax: false,
			countDirection: 'up',
			safeClass: 'safe',
			overClass: 'over',
			thousandSeparator: ' '
		});

		var timer;

		$source.keyup(function() {
			clearTimeout(timer);
			timer = setTimeout(function() {
				translit();
			}, 300);
		});

		$standard
			.add($space)
			.add($register)
			.add($alphanum)
			.change(function() {
				translit();
			});

		$standard
			.change(function() {
				var standardId = $(this).val();
				if (standardId !== '') {
					render(standardId);
				} else {
					$view.empty();
				}
			})
			.trigger('change');
	});

	function translit() {
		var text = $.trim($source.val()),
			standardId = $standard.val(),
			rules = standardId !== '' ? standards[standardId].rules : null,
			spaceValue = $space.val(),
			registerValue = $register.val(),
			alphanum = $alphanum.is(':checked'),
			result = '',
			length = text.length,
			char,
			replaceChar,
			i;

		if (text !== '') {
			if (rules !== null) {
				for (i = 0; i < length; i++) {
					char = text.charAt(i);
					if (char.toLowerCase() in rules) {
						replaceChar = rules[char.toLowerCase()];
						if (isUpperCase(char)) {
							replaceChar = capitalizeFirstLetter(replaceChar);
						}
						result += replaceChar;
					} else {
						result += char;
					}
				}
			} else {
				result = text;
			}

			if (alphanum) {
				result = result.replace(/[^-_0-9a-zA-Zа-яёА-ЯЁ\s]/g, '');
				result = result.replace(/ {2,}/g, ' ');
			}

			if (spaceValue == SPACE_DASH) {
				result = result.replace(/ /g, '-');
				result = result.replace(/-{2,}/g, '-');
			} else if (spaceValue == SPACE_UNDERLINE) {
				result = result.replace(/ /g, '_');
				result = result.replace(/_{2,}/g, '_');
			}

			if (registerValue == REGISTER_LOWER) {
				result = result.toLowerCase();
			} else if (registerValue == REGISTER_UPPER) {
				result = result.toUpperCase();
			}
		}

		$target
			.val($.trim(result))
			// обновляем счетчик символов
			.trigger('keyup');
	}

	function isUpperCase(char) {
		return char.toUpperCase() == char;
	}

	function capitalizeFirstLetter(string) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	function render(standardId) {
		var standard = standards[standardId].rules;

		var key,
			html = '<table class="table table-bordered table-striped"><tr>';

		for (key in standard) {
			html += '<td>' + key + '</td>';
		}
		html += '</tr><tr>';
		for (key in standard) {
			html += '<td>' + standard[key] + '</td>';
		}
		html += '</tr></table>';

		$view.html(html);
	}

	function buildStandardSelect() {
		var standardId,
			html = '<option value="">- нет -</option>';

		for (standardId in standards) {
			if (standards.hasOwnProperty(standardId)) {
				html += '<option value="' + standardId + '">' + standards[standardId].name + '</option>';
			}
		}

		$standard.html(html);

		// выбираем первый стандарт
		for (standardId in standards) {
			if (standards.hasOwnProperty(standardId)) {
				$standard.val(standardId);
				break;
			}
		}
	}

	function buildStandardsInfo() {
		var standardId,
			html = '';

		for (standardId in standards) {
			if (!standards.hasOwnProperty(standardId) || !standards[standardId].description) {
				continue;
			}
			html += '<p><b>' + standards[standardId].name + '</b> — ' + standards[standardId].description + '</p>';
		}

		$('#standards-info').html(html);
	}

})(jQuery, standards || {});
