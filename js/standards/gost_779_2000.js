var standards = standards || {};

standards.gost_779_2000 = {
	name: 'ГОСТ 7.79—2000 Б',
	description: 'стандарт, являющийся адаптацией ISO 9, принятый в России и некоторых странах СНГ. ISO 9 — ' +
		'международный стандарт, определяющий систему транслитерации кириллических алфавитов славянских и неславянских ' +
		'языков посредством латиницы.',
	rules: {
		'а': 'a',
		'б': 'b',
		'в': 'v',
		'г': 'g',
		'д': 'd',
		'е': 'e',
		'ё': 'yo',
		'ж': 'zh',
		'з': 'z',
		'и': 'i',
		'й': 'j',
		'к': 'k',
		'л': 'l',
		'м': 'm',
		'н': 'n',
		'о': 'o',
		'п': 'p',
		'р': 'r',
		'с': 's',
		'т': 't',
		'у': 'u',
		'ф': 'f',
		'х': 'x',
		'ц': 'cz',
		'ч': 'ch',
		'ш': 'sh',
		'щ': 'shh',
		'ъ': '``',
		'ы': 'y`',
		'ь': '`',
		'э': 'e`',
		'ю': 'yu',
		'я': 'ya'
	}
};